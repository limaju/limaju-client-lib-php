<?php
/**
 * ResultReadTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  MjOpenApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Majority Judgment API
 *
 * This is a **deliberation service** using (liquid) **majority judgment** polling. It's **libre software** made and maintained by [MieuxVoter.fr](https://mieuxvoter.fr). You're browsing the API documentation.  For more, head over to [Gitlab](https://framagit.org/limaju).
 *
 * The version of the OpenAPI document: 0.0.0-dev
 * 
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace MjOpenApi;

use PHPUnit\Framework\TestCase;

/**
 * ResultReadTest Class Doc Comment
 *
 * @category    Class
 * @description A Result of a (Liquid) Majority Judgment Poll.
 * @package     MjOpenApi
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class ResultReadTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "ResultRead"
     */
    public function testResultRead()
    {
    }

    /**
     * Test attribute "poll"
     */
    public function testPropertyPoll()
    {
    }

    /**
     * Test attribute "algorithm"
     */
    public function testPropertyAlgorithm()
    {
    }

    /**
     * Test attribute "leaderboard"
     */
    public function testPropertyLeaderboard()
    {
    }
}
