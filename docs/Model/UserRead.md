# # UserRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **string** |  | [optional] [readonly] 
**email** | **string** |  | [optional] 
**username** | **string** |  | [optional] 
**polls** | [**\MjOpenApi\Model\PollRead[]**](PollRead.md) |  | [optional] [readonly] 
**ballots** | [**\MjOpenApi\Model\BallotRead[]**](BallotRead.md) |  | [optional] [readonly] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


