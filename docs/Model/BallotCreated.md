# # BallotCreated

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **string** |  | [optional] 
**proposal** | **string** | The Majority Judgment Poll Proposal the author is giving a grade to. | [optional] 
**grade** | **string** | The Grade attributed by the Judge to the Proposal. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


