# # ResultRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**poll** | [**\MjOpenApi\Model\PollRead**](PollRead.md) |  | [optional] 
**algorithm** | **string** | The name of the algorithm used to derive this Result.  \\n Default: \&quot;standard\&quot;. | [optional] 
**leaderboard** | [**\MjOpenApi\Model\ProposalResultRead[]**](ProposalResultRead.md) | In order, each proposals&#39; tally.  \\n Some proposals, in extreme, low-participation polls, may have the same rank.  \\n In that case, their order should be the order they were defined in the poll. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


