# # ProposalResultRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**proposal** | [**\MjOpenApi\Model\ProposalRead**](ProposalRead.md) |  | [optional] 
**rank** | **int** | The computed rank of the Proposal in the Poll.  \\n Rank starts at 1 and goes upwards.  \\n Two proposals may have the same rank. | [optional] 
**median_grade** | [**\MjOpenApi\Model\GradeRead**](GradeRead.md) |  | [optional] 
**tally** | **int** | Total Amount of Ballots emitted for the Proposal this Result is about.  \\n This includes the \&quot;ghost\&quot;, default ballots. | [optional] 
**grades_results** | [**\MjOpenApi\Model\ProposalGradeResultRead[]**](ProposalGradeResultRead.md) | Results for each Grade, on this Proposal.  \\n This is the merit profile of the Proposal. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


