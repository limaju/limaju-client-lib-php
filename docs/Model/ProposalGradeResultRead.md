# # ProposalGradeResultRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**grade** | **string** | The Grade this Result is about. | [optional] 
**proposal** | **string** | The Proposal this Result is about. | [optional] 
**tally** | **int** | Amount of Ballots emitted for this Grade on the Proposal. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


