# # BallotCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**grade** | **string** | The Grade attributed by the Judge to the Proposal. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


