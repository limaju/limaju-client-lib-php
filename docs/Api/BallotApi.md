# MjOpenApi\BallotApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiPollsProposalsBallotsGetSubresource**](BallotApi.md#apiPollsProposalsBallotsGetSubresource) | **GET** /polls/{id}/proposals/{proposals}/ballots | Retrieves the collection of Ballot resources.
[**apiProposalsBallotsGetSubresource**](BallotApi.md#apiProposalsBallotsGetSubresource) | **GET** /proposals/{id}/ballots | Retrieves the collection of Ballot resources.
[**deleteBallotItem**](BallotApi.md#deleteBallotItem) | **DELETE** /ballots/{id} | Removes the Ballot resource.
[**getBallotItem**](BallotApi.md#getBallotItem) | **GET** /ballots/{id} | Retrieves a Ballot resource.
[**postBallotCollection**](BallotApi.md#postBallotCollection) | **POST** /polls/{pollId}/proposals/{proposalId}/ballots | Creates a Ballot resource.



## apiPollsProposalsBallotsGetSubresource

> \MjOpenApi\Model\BallotRead[] apiPollsProposalsBallotsGetSubresource($id, $proposals)

Retrieves the collection of Ballot resources.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKey
$config = MjOpenApi\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = MjOpenApi\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new MjOpenApi\Api\BallotApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 'id_example'; // string | 
$proposals = 'proposals_example'; // string | 

try {
    $result = $apiInstance->apiPollsProposalsBallotsGetSubresource($id, $proposals);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BallotApi->apiPollsProposalsBallotsGetSubresource: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |
 **proposals** | **string**|  |

### Return type

[**\MjOpenApi\Model\BallotRead[]**](../Model/BallotRead.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## apiProposalsBallotsGetSubresource

> \MjOpenApi\Model\BallotRead[] apiProposalsBallotsGetSubresource($id)

Retrieves the collection of Ballot resources.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKey
$config = MjOpenApi\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = MjOpenApi\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new MjOpenApi\Api\BallotApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 'id_example'; // string | 

try {
    $result = $apiInstance->apiProposalsBallotsGetSubresource($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BallotApi->apiProposalsBallotsGetSubresource: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

[**\MjOpenApi\Model\BallotRead[]**](../Model/BallotRead.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteBallotItem

> deleteBallotItem($id)

Removes the Ballot resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKey
$config = MjOpenApi\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = MjOpenApi\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new MjOpenApi\Api\BallotApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 'id_example'; // string | 

try {
    $apiInstance->deleteBallotItem($id);
} catch (Exception $e) {
    echo 'Exception when calling BallotApi->deleteBallotItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

void (empty response body)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getBallotItem

> \MjOpenApi\Model\BallotRead getBallotItem($id)

Retrieves a Ballot resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKey
$config = MjOpenApi\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = MjOpenApi\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new MjOpenApi\Api\BallotApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 'id_example'; // string | 

try {
    $result = $apiInstance->getBallotItem($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BallotApi->getBallotItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

[**\MjOpenApi\Model\BallotRead**](../Model/BallotRead.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## postBallotCollection

> \MjOpenApi\Model\BallotCreated postBallotCollection($poll_id, $proposal_id, $ballot_create)

Creates a Ballot resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKey
$config = MjOpenApi\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = MjOpenApi\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new MjOpenApi\Api\BallotApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$poll_id = 'poll_id_example'; // string | 
$proposal_id = 'proposal_id_example'; // string | 
$ballot_create = new \MjOpenApi\Model\BallotCreate(); // \MjOpenApi\Model\BallotCreate | The new Ballot resource

try {
    $result = $apiInstance->postBallotCollection($poll_id, $proposal_id, $ballot_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BallotApi->postBallotCollection: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **poll_id** | **string**|  |
 **proposal_id** | **string**|  |
 **ballot_create** | [**\MjOpenApi\Model\BallotCreate**](../Model/BallotCreate.md)| The new Ballot resource | [optional]

### Return type

[**\MjOpenApi\Model\BallotCreated**](../Model/BallotCreated.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

- **Content-Type**: application/ld+json, application/json, text/html
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

