# MjOpenApi\ProposalGradeResultApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getProposalGradeResultItem**](ProposalGradeResultApi.md#getProposalGradeResultItem) | **GET** /proposal_grade_results/{id} | Retrieves a ProposalGradeResult resource.



## getProposalGradeResultItem

> \MjOpenApi\Model\ProposalGradeResultRead getProposalGradeResultItem($id)

Retrieves a ProposalGradeResult resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKey
$config = MjOpenApi\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = MjOpenApi\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new MjOpenApi\Api\ProposalGradeResultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 'id_example'; // string | 

try {
    $result = $apiInstance->getProposalGradeResultItem($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProposalGradeResultApi->getProposalGradeResultItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

[**\MjOpenApi\Model\ProposalGradeResultRead**](../Model/ProposalGradeResultRead.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

